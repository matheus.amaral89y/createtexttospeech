package com.hering.desafiojava.services;

import com.hering.desafiojava.core.services.TextToSpeechService;
import com.hering.desafiojava.core.services.model.TextToSpeechMessage;
import com.hering.desafiojava.core.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class TextToSpeechConsumerService {

    @Autowired
    private TextToSpeechService service;

    @KafkaListener(topics = "speech", groupId = "speechTextGroup")
    public void consumer(String message) {
       TextToSpeechMessage model = Utils.deserialize(TextToSpeechMessage.class, message);
        service.createSpeech(model.getLanguage(), model.getVoice(), model.getText());

    }
}
