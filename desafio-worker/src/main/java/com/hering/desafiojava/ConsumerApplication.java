package com.hering.desafiojava;

import com.hering.desafiojava.core.repository.TextToSpeechRepository;
import com.hering.desafiojava.core.services.TextToSpeechService;
import com.hering.desafiojava.core.services.model.TextToSpeechMessage;
import com.hering.desafiojava.services.TextToSpeechConsumerService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackageClasses = { TextToSpeechService.class, TextToSpeechConsumerService.class, TextToSpeechMessage.class})
@EnableJpaRepositories(basePackageClasses = {TextToSpeechRepository.class})
public class ConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class, args);
    }
}