FROM eclipse-temurin:17-jdk AS build-env

ADD . /sources
WORKDIR /sources

RUN chmod +x ./mvnw

RUN ./mvnw clean package

FROM eclipse-temurin:17-jre

WORKDIR /usr/local/app
COPY --from=build-env /sources/desafio-api/target/desafio-api-*.jar ./api.jar
COPY --from=build-env /sources/desafio-core/target/*.jar ./core.jar
COPY --from=build-env /sources/desafio-worker/target/desafio-worker-*.jar ./worker.jar

CMD ["java"]