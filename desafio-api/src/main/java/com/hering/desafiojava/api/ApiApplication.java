package com.hering.desafiojava.api;

import com.hering.desafiojava.api.config.WebMvcConfig;
import com.hering.desafiojava.api.controller.HomeController;
import com.hering.desafiojava.api.controller.TextToSpeechController;
import com.hering.desafiojava.core.entities.Producer;
import com.hering.desafiojava.core.entities.TextToSpeech;
import com.hering.desafiojava.core.repository.TextToSpeechRepository;
import com.hering.desafiojava.core.services.TextToSpeechService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackageClasses = {WebMvcConfig.class, TextToSpeechService.class, TextToSpeechController.class, HomeController.class, Producer.class })
@EnableJpaRepositories(basePackageClasses = { TextToSpeechRepository.class })
@EntityScan(basePackageClasses = { TextToSpeech.class })
public class ApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiApplication.class, args);
    }

}
