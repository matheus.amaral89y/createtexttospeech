function toggleDropdownSpeechId() {
    var dropdownContent = document.getElementById("dropdownContent-speechId");
    dropdownContent.style.display = (dropdownContent.style.display === "none" || dropdownContent.style.display === "") ? "block" : "none";
}

function toggleDropdownSpeechIdAudio() {
    var dropdownContent = document.getElementById("dropdownContent-speechIdAudio");
    dropdownContent.style.display = (dropdownContent.style.display === "none" || dropdownContent.style.display === "") ? "block" : "none";
}

function toggleDropdownSpeechSync() {
    var dropdownContent = document.getElementById("dropdownContent-speechSync");
    dropdownContent.style.display = (dropdownContent.style.display === "none" || dropdownContent.style.display === "") ? "block" : "none";
}

function toggleDropdownSpeechAsync() {
    var dropdownContent = document.getElementById("dropdownContent-speechAsync");
    dropdownContent.style.display = (dropdownContent.style.display === "none" || dropdownContent.style.display === "") ? "block" : "none";
}

function toggleDropdownSpeechStatus() {
    var dropdownContent = document.getElementById("dropdownContent-speechStatus");
    dropdownContent.style.display = (dropdownContent.style.display === "none" || dropdownContent.style.display === "") ? "block" : "none";
}

function searchById() {
    var input = document.getElementById("speechId");

    var id = input.value;
    var apiUrl = "http://localhost:8080/api/v1/text-to-speech/" + id;

    fetch(apiUrl)
        .then(response => {
            if (!response.ok) {
                throw new Error(`Erro HTTP: ${response.status}`);
            }

            return response.json();
        })
        .then(data => {
            console.log("Dados da API:", data);

            var dynamicDiv = document.createElement("div");

            dynamicDiv.classList.add("dynamic-div");
            dynamicDiv.innerHTML = "Dados da API: " + JSON.stringify(data);

            var divSpeechId = document.getElementById("dropdownContent-speechId");

            divSpeechId.appendChild(dynamicDiv);
        })
        .catch(error => {
            console.error("Erro durante a requisição:", error.message);
        });
}

function searchByIdAudio() {
    var input = document.getElementById("speechIdAudio");

    var id = input.value;
    var apiUrl = "http://localhost:8080/api/v1/text-to-speech/" + id + "/audio";

    fetch(apiUrl)
        .then(response => {
            if (!response.ok) {
                throw new Error(`Erro HTTP: ${response.status}`);
            }

            return response.blob();
        })
        .then(data => {
            console.log("Áudio recebido:", data);

            var dynamicDiv = document.createElement("div");
            dynamicDiv.classList.add("dynamic-div");

            var audioElement = document.createElement("audio");
            audioElement.src = URL.createObjectURL(data);
            audioElement.controls = true;

            var divSpeechId = document.getElementById("dropdownContent-speechIdAudio");

            divSpeechId.appendChild(dynamicDiv);
            dynamicDiv.appendChild(audioElement);
        })
        .catch(error => {
            console.error("Erro durante a requisição:", error.message);
        });
}

function saveSync() {
    var inputText = document.getElementById("speechText");
    var inputLanguage = document.getElementById("speechLanguage");
    var inputVoice = document.getElementById("speechVoice");

    var text = inputText.value;
    var language = inputLanguage.value;
    var voice = inputVoice.value;

    var apiUrl = "http://localhost:8080/api/v1/text-to-speech/sync?text=" + text + "&language=" + language + "&voice=" + voice;

    fetch(apiUrl)
        .then(response => {
            if (!response.ok) {
                throw new Error(`Erro HTTP: ${response.status}`);
            }

            return response.json();
        })
        .then(data => {
            console.log("Dados do cadastro:", data);

            var dynamicDiv = document.createElement("div");

            dynamicDiv.classList.add("dynamic-div");
            dynamicDiv.innerHTML = "Dados do cadastro: " + JSON.stringify(data);

            var divSpeechId = document.getElementById("dropdownContent-speechSync");

            divSpeechId.appendChild(dynamicDiv);
        })
        .catch(error => {
            console.error("Erro durante a requisição:", error.message);
        });
}

function saveAsync() {
    var inputText = document.getElementById("speechTextAsync");
    var inputLanguage = document.getElementById("speechLanguageAsync");
    var inputVoice = document.getElementById("speechVoiceAsync");

    var text = inputText.value;
    var language = inputLanguage.value;
    var voice = inputVoice.value;

    var apiUrl = "http://localhost:8080/api/v1/text-to-speech/async?text=" + text + "&language=" + language + "&voice=" + voice;

    fetch(apiUrl)
        .then(response => {
            if (!response.ok) {
                throw new Error(`Erro HTTP: ${response.status}`);
            }

            return response.json();
        })
        .then(data => {
            console.log("Dados do cadastro:", data);

            var dynamicDiv = document.createElement("div");

            dynamicDiv.classList.add("dynamic-div");
            dynamicDiv.innerHTML = "Dados do cadastro: " + JSON.stringify(data);

            var divSpeechId = document.getElementById("dropdownContent-speechAsync");

            divSpeechId.appendChild(dynamicDiv);
        })
        .catch(error => {
            console.error("Erro durante a requisição:", error.message);
        });
}

function searchByStatus() {
    var statusSelect = document.getElementById("speechStatus");
    var selectedOption = statusSelect.options[statusSelect.selectedIndex];
    var selectedStatus = selectedOption.value;
    var selectedDescription = selectedOption.text;

    var apiUrl = "http://localhost:8080/api/v1/text-to-speech/status?statusList=" + selectedDescription;

    fetch(apiUrl)
        .then(response => {
            if (!response.ok) {
                throw new Error(`Erro HTTP: ${response.status}`);
            }

            return response.json();
        })
        .then(data => {
            console.log("Dados da API:", data);

            var dynamicDiv = document.createElement("div");

            dynamicDiv.classList.add("dynamic-div");
            dynamicDiv.innerHTML = "Dados da API: " + JSON.stringify(data);

            var divSpeechId = document.getElementById("dropdownContent-speechStatus");

            divSpeechId.appendChild(dynamicDiv);
        })
        .catch(error => {
            console.error("Erro durante a requisição:", error.message);
        });
}
