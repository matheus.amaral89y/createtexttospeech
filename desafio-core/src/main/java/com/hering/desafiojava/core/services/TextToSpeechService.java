package com.hering.desafiojava.core.services;

import com.hering.desafiojava.core.entities.TextToSpeech;
import com.hering.desafiojava.core.entities.TextToSpeechStatus;
import com.hering.desafiojava.core.repository.TextToSpeechRepository;
import com.hering.desafiojava.core.services.model.HttpResponseModel;
import com.hering.desafiojava.core.services.model.TextToSpeechMessage;
import com.hering.desafiojava.core.services.model.TextToSpeechModel;
import com.hering.desafiojava.core.utils.HttpUtils;
import com.hering.desafiojava.core.utils.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class TextToSpeechService {
    private static final String URL_FORMAT = "https://api.voicerss.org/?key=b52ad1e1a7ea44e799babb04ff4d229f&hl=%s&v=%s&src=%s";
    private final TextToSpeechRepository textToSpeechRepository;
    private final String ERROR = "ERROR";

    public TextToSpeechService(TextToSpeechRepository textToSpeechRepository) {
        this.textToSpeechRepository = textToSpeechRepository;
    }

    public TextToSpeechModel createSpeech(String language, String voice, String text){
        String url = String.format(URL_FORMAT, language, voice, encodeValue(text));

        var model = TextToSpeechModel.builder()
                .momment(Instant.now())
                .language(language)
                .text(text)
                .voice(voice)
                .status(TextToSpeechStatus.PROCESSING)
                .build();

        var entity = model.toEntity();

        save(model, entity);

        try {
            var response = HttpUtils.get(url);

            var error = Utils.convertInputStreamToString(Utils.convertBytesToInputStream(response.getResponse()));

            if (response.getStatusCode() != 200 || (!error.isBlank() && error.contains(ERROR))){
                var message = "Ocorreu um problema ao criar o discurso. %s";

                setError(model, entity, String.format(message, error));
                log.error(String.format(message, error));

                return save(model, entity);
            }
            String encoded = Base64.getEncoder().encodeToString(response.getResponse());
            model.setStatus(TextToSpeechStatus.COMPLETED);

            entity.setAudioWavBase64(encoded);
            entity.setStatus(model.getStatus());

            return save(model, entity);

        } catch (ConnectException e){
            setError(model, entity, "Ocorreu um erro de conexão");
            save(model, entity);

            log.error("Ocorreu um erro de conexão {}", model, e);
            throw new RuntimeException(e);

        } catch (URISyntaxException | IOException | InterruptedException e) {
            setError(model, entity, "Ocorreu um erro");
            save(model, entity);

            log.error("Ocorreu um erro {}", model, e);
            throw new RuntimeException(e);
        }

    }

    public TextToSpeechModel search(Long id) {
        var entity = textToSpeechRepository.findById(id).orElse(null);

        if(entity == null)
            return null;

        return TextToSpeechModel.fromEntity(entity);
    }

    private String encodeValue(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public byte[] searchAudio(Long id) {
        var optional = textToSpeechRepository.findById(id);
        var entity = optional.orElse(null);

        if(entity == null){
            return null;
        }

        return Base64.getDecoder().decode(entity.getAudioWavBase64());
    }

    public List<TextToSpeechModel> searchByStatus(List<String> statusList){
        List<TextToSpeechStatus> enumStatusList = statusList.stream()
                .map(TextToSpeechStatus::valueOf)
                .collect(Collectors.toList());

        List<TextToSpeech> textToSpeechList = textToSpeechRepository.findByStatusIn(enumStatusList);
        List<TextToSpeechModel> textToSpeechModelList = new ArrayList<>();

        for (TextToSpeech toSpeech : textToSpeechList){
            var model = TextToSpeechModel.fromEntity(toSpeech);

            textToSpeechModelList.add(model);
        }

        return textToSpeechModelList;
    }

    private TextToSpeechModel save(TextToSpeechModel model, TextToSpeech entity){
        entity = textToSpeechRepository.save(entity);
        model.setId(entity.getId());

        return model;
    }

    private void setError(TextToSpeechModel model, TextToSpeech entity, String message){
        model.setStatus(TextToSpeechStatus.ERROR);

        entity.setErrorText(message);
        entity.setStatus(model.getStatus());
    }

    public TextToSpeechModel createSpeechAsync(String language, String voice, String text){
        return TextToSpeechModel.builder()
                .language(language)
                .voice(voice)
                .text(text)
                .status(TextToSpeechStatus.PROCESSING)
                .id(0L)
                .build();
    }

    public TextToSpeechMessage createMessageFromTextSpeechModel(TextToSpeechModel model){
        var message = new TextToSpeechMessage();
        message.setLanguage(model.getLanguage());
        message.setVoice(model.getVoice());
        message.setText(model.getText());
        message.setStatus(model.getStatus());

        return message;
    }
}
