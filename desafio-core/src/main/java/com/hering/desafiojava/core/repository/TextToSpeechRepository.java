package com.hering.desafiojava.core.repository;

import com.hering.desafiojava.core.entities.TextToSpeech;
import com.hering.desafiojava.core.entities.TextToSpeechStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TextToSpeechRepository extends JpaRepository<TextToSpeech, Long> {
    @Query("SELECT tts FROM TextToSpeech tts WHERE tts.status IN :statusList")
    List<TextToSpeech> findByStatusIn(@Param("statusList") List<TextToSpeechStatus> statusList);
}
