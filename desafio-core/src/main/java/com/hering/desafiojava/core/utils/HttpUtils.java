package com.hering.desafiojava.core.utils;

import com.hering.desafiojava.core.services.model.HttpResponseModel;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class HttpUtils {
    private static final HttpClient HTTP_CLIENT = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_1_1)
            .build();

    private HttpUtils(){

    }

    public static HttpResponseModel get(String url) throws URISyntaxException, IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI(url))
                .GET()
                .build();

        var httpResponse = HTTP_CLIENT.send(request, HttpResponse.BodyHandlers.ofInputStream());

        InputStream is = httpResponse.body();
        byte[] response = is.readAllBytes();

        return new HttpResponseModel(httpResponse.statusCode(), response);
    }
}
