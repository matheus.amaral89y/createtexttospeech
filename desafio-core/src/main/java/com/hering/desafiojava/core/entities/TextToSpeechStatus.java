package com.hering.desafiojava.core.entities;

public enum TextToSpeechStatus {
    PROCESSING("PROCESSING"),
    COMPLETED("COMPLETED"),
    ERROR("ERROR");

    private final String descricao;

    TextToSpeechStatus(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
