package com.hering.desafiojava.core.services.model;

import com.hering.desafiojava.core.entities.TextToSpeechStatus;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TextToSpeechMessage {
    private String language;
    private String voice;
    private String text;
    private TextToSpeechStatus status;
}
