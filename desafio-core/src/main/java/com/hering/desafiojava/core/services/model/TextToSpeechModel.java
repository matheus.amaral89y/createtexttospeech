package com.hering.desafiojava.core.services.model;

import com.hering.desafiojava.core.entities.TextToSpeech;
import com.hering.desafiojava.core.entities.TextToSpeechStatus;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.Instant;

@Getter
@Setter
@Builder
@ToString
public class TextToSpeechModel {
    private Long id;
    private Instant momment;
    private String language;
    private String voice;
    private String text;
    private TextToSpeechStatus status;

    public TextToSpeech toEntity(){
        var entity = new TextToSpeech();

        entity.setMomment(getMomment());
        entity.setLanguage(getLanguage());
        entity.setVoice(getVoice());
        entity.setText(getText());
        entity.setStatus(getStatus());
        entity.setId(getId());

        return entity;
    }

    public static TextToSpeechModel fromEntity(TextToSpeech entity){

        return TextToSpeechModel.builder()
                .id(entity.getId())
                .momment(entity.getMomment())
                .language(entity.getLanguage())
                .text(entity.getText())
                .status(entity.getStatus())
                .voice(entity.getVoice())
                .build();
    }
}
