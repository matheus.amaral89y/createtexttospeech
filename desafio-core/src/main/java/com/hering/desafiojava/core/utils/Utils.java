package com.hering.desafiojava.core.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.util.stream.Collectors;

public class Utils {
    private Utils(){

    }
    public static String convertInputStreamToString(InputStream inputStream) throws IOException {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
            return bufferedReader.lines().collect(Collectors.joining("\n"));
        }
    }

    public static InputStream convertBytesToInputStream(byte[] bytes) {
        return new ByteArrayInputStream(bytes);
    }

    public static <T> T deserialize(Class<T> clazz, String json) {
        try {

            return new ObjectMapper().readValue(json, clazz);

        } catch (JsonProcessingException e) {
            String errorMessage = String.format("Erro ao deserializar json %s para %s", json, clazz);

            throw new RuntimeException(errorMessage, e);
        }
    }
}
