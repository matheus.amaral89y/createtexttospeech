package com.hering.desafiojava.core.entities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class Producer<T> {

    @Autowired
    private KafkaTemplate<String, T> kafkaTemplate;

    public void send(String topic, T message) {
        kafkaTemplate.send(topic, message);
    }
}
