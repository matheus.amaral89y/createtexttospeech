package com.hering.desafiojava.core.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class TextToSpeech extends EntityBase{
    @Column
    private String language;
    @Column
    private String voice;
    @Column
    private String text;
    @Enumerated(EnumType.STRING)
    private TextToSpeechStatus status;
    @Column
    private String errorText;
    @Column
    private String audioWavBase64;
}
