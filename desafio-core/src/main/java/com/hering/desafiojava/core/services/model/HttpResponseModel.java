package com.hering.desafiojava.core.services.model;

import lombok.Getter;

@Getter
public class HttpResponseModel {
    private int statusCode;
    private byte[] response;

    public HttpResponseModel(int statusCode, byte[] response){
        this.statusCode = statusCode;
        this.response = response;
    }
}
